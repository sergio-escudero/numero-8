import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GaleriaComponent } from './componentes/routes/galeria/galeria.component';
import { InicioComponent } from './componentes/routes/inicio/inicio.component';
import { ServiciosComponent } from './componentes/routes/servicios/servicios.component';

const routes: Routes =
 [
  {
    path:'inicio',
    component:InicioComponent
  },
  {
    path:'galeria',
    component:GaleriaComponent
  },
  {
    path:'servicios',
    component:ServiciosComponent
  },
  {
    path:'**',
   redirectTo:'inicio'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
