import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { GaleriaComponent } from './galeria/galeria.component';




@NgModule({
  declarations: [
    InicioComponent,
    ServiciosComponent,
    GaleriaComponent,
    
  ],
  imports: [
    CommonModule
  ],
  exports:[
    InicioComponent,
    ServiciosComponent,
    GaleriaComponent,
  
  ],
})
export class RoutesModule { }
